﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadioNetworkDesktop.Models
{
    class RadioData
    {
        private string name;
        private string action;
        private string value;

        public RadioData(string name, string action, string value)
        {
            this.name = name;
            this.action = action;
            this.value = value;
        }

        public string Name { get => name; set => name = value; }
        public string Action { get => action; set => action = value; }
        public string Value { get => value; set => this.value = value; }
    }
}
