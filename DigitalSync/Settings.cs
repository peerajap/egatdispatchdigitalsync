﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DigitalSync
{
    public partial class Settings : Form
    {
        static Settings MsgBox;
        static DialogResult result = DialogResult.No;

        public Settings()
        {
            InitializeComponent();
            InitialData();

        }

        private void InitialData()
        {
            textBoxURL.Text = Properties.Settings.Default.SocketURL;

        }

        public static DialogResult MSGShow()
        {
            MsgBox = new Settings();
            MsgBox.ShowDialog();
            return result;
        }

        private void ConnectButton_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.SocketURL = textBoxURL.Text;

            Properties.Settings.Default.Save();
            result = DialogResult.OK;
            MsgBox.Close();
        }

        private void DisconnectButton_Click(object sender, EventArgs e)
        {
            result = DialogResult.No;
            MsgBox.Close();
        }
    }
}
