﻿using DigitalSync.DB;
using DigitalSync.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DigitalSync
{
    public partial class ProfileUpdate : Form
    {
        static ProfileUpdate MsgBox;
        static DialogResult result = DialogResult.No;
        private ButtonFuncUserControl buttonFuncUser;

        public ProfileUpdate(ButtonFuncUserControl buttonFuncUser)
        {
            this.buttonFuncUser = buttonFuncUser;
            InitializeComponent();
            // InitialData();
            if (this.buttonFuncUser.ButtonMode == "UID")
            {
                InitialData_UID();
            }
            else 
            {
                InitialData_Group();
            }
            //InitialData();
        }

        private void InitialData()
        {
            textID.Text = buttonFuncUser.profile.Id.ToString();
            textGroup.Text = buttonFuncUser.profile.Group.ToString();
            textName.Text = buttonFuncUser.profile.Name;
            textRemark.Text = buttonFuncUser.profile.Remark;

        }

        private void InitialData_UID() 
        {
            textID.Text = buttonFuncUser.profile.Id.ToString();
            this.label2.Text = "UID";
            textGroup.Text = buttonFuncUser.profile.UID.ToString();
            textName.Text = buttonFuncUser.profile.Name;
        }
        private void InitialData_Group() 
        {
            textID.Text = buttonFuncUser.profileGroup.Id.ToString();
            this.label2.Text = "Group";
            textGroup.Text = buttonFuncUser.profileGroup.Group.ToString();
            textName.Text = buttonFuncUser.profileGroup.Name;
        }
        public static DialogResult MSGShow(ButtonFuncUserControl buttonFuncUser)
        {
            MsgBox = new ProfileUpdate(buttonFuncUser);
            MsgBox.ShowDialog();
            return result;
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            result = DialogResult.No;
            MsgBox.Close();
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            result = DialogResult.OK;

            try
            {
                //buttonFuncUser.profile.Group = Int32.Parse(textGroup.Text);
                //buttonFuncUser.profile.Name = textName.Text.ToString();
                //buttonFuncUser.profile.Remark = textRemark.Text.ToString();

                //Profile.Update(buttonFuncUser.profile);
                if (this.buttonFuncUser.ButtonMode == "UID")
                {
                    this.buttonFuncUser.profile.UID = Int32.Parse(textGroup.Text);
                    this.buttonFuncUser.profile.Name = textName.Text;
                    Profile.Update(this.buttonFuncUser.profile);
                }
                else
                {

                    this.buttonFuncUser.profileGroup.Group = Int32.Parse(textGroup.Text);
                    this.buttonFuncUser.profileGroup.Name = textName.Text;
                    ProfileGroup.Update(this.buttonFuncUser.profileGroup);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                MessageBox.Show("Can not update data, Please check input again.", "Warning!!!", MessageBoxButtons.OK);
            }

            MsgBox.Close();
        }
    }
}
