﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace DigitalSync
{
    public partial class WebMap : UserControl
    {
        private MainForm mainForm;
        public WebMap(MainForm mainForm)
        {
            this.mainForm = mainForm;
            InitializeComponent();
        }

        private void WebMap_Resize(object sender, EventArgs e)
        {
            Dock = DockStyle.Fill;
            webView.Dock = DockStyle.Fill;
            Debug.WriteLine("WebMap_Resize");
        }


        private void webView21_Resize(object sender, EventArgs e)
        {
            Dock = DockStyle.Fill;
            webView.Dock = DockStyle.Fill;
            Debug.WriteLine("webView21_Resize");

        }

        public void Reinit() 
        {
            webView.Refresh();
            webView.Source = new System.Uri(" https://192.168.8.2", System.UriKind.Absolute);
        }

        public void go_to_gooole() 
        {
            webView.Source=new System.Uri(" https://www.google.com", System.UriKind.Absolute);
        }
     
    }
}
