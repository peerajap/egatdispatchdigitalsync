﻿
namespace DigitalSync
{
    partial class ActionFrom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ActionFrom));
            this.labelSys = new System.Windows.Forms.Label();
            this.textBoxSys = new System.Windows.Forms.TextBox();
            this.labelName = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.labelCMD = new System.Windows.Forms.Label();
            this.comboBoxCMD = new System.Windows.Forms.ComboBox();
            this.butSendCMD = new System.Windows.Forms.Button();
            this.labelMD = new System.Windows.Forms.Label();
            this.textMD = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBoxTM = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // labelSys
            // 
            this.labelSys.AutoSize = true;
            this.labelSys.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelSys.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelSys.Location = new System.Drawing.Point(41, 35);
            this.labelSys.Name = "labelSys";
            this.labelSys.Size = new System.Drawing.Size(105, 38);
            this.labelSys.TabIndex = 0;
            this.labelSys.Text = "System";
            // 
            // textBoxSys
            // 
            this.textBoxSys.Enabled = false;
            this.textBoxSys.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxSys.Location = new System.Drawing.Point(152, 35);
            this.textBoxSys.Name = "textBoxSys";
            this.textBoxSys.ReadOnly = true;
            this.textBoxSys.Size = new System.Drawing.Size(614, 43);
            this.textBoxSys.TabIndex = 1;
            this.textBoxSys.WordWrap = false;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelName.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelName.Location = new System.Drawing.Point(41, 116);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(91, 38);
            this.labelName.TabIndex = 2;
            this.labelName.Text = "Name";
            // 
            // textBoxName
            // 
            this.textBoxName.Enabled = false;
            this.textBoxName.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxName.Location = new System.Drawing.Point(152, 116);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.ReadOnly = true;
            this.textBoxName.Size = new System.Drawing.Size(614, 43);
            this.textBoxName.TabIndex = 3;
            this.textBoxName.WordWrap = false;
            // 
            // labelCMD
            // 
            this.labelCMD.AutoSize = true;
            this.labelCMD.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelCMD.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelCMD.Location = new System.Drawing.Point(41, 216);
            this.labelCMD.Name = "labelCMD";
            this.labelCMD.Size = new System.Drawing.Size(239, 38);
            this.labelCMD.TabIndex = 4;
            this.labelCMD.Text = "Special Command";
            // 
            // comboBoxCMD
            // 
            this.comboBoxCMD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCMD.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.comboBoxCMD.FormattingEnabled = true;
            this.comboBoxCMD.Items.AddRange(new object[] {
            "Stun",
            "Revive",
            "Kill",
            "Remote Monitor"});
            this.comboBoxCMD.Location = new System.Drawing.Point(50, 271);
            this.comboBoxCMD.Name = "comboBoxCMD";
            this.comboBoxCMD.Size = new System.Drawing.Size(564, 45);
            this.comboBoxCMD.TabIndex = 5;
            this.comboBoxCMD.SelectedIndexChanged += new System.EventHandler(this.comboBoxCMD_SelectedIndexChanged);
            // 
            // butSendCMD
            // 
            this.butSendCMD.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.butSendCMD.Location = new System.Drawing.Point(649, 271);
            this.butSendCMD.Name = "butSendCMD";
            this.butSendCMD.Size = new System.Drawing.Size(130, 45);
            this.butSendCMD.TabIndex = 6;
            this.butSendCMD.Text = "Send";
            this.butSendCMD.UseVisualStyleBackColor = true;
            this.butSendCMD.Click += new System.EventHandler(this.butSendCMD_Click);
            // 
            // labelMD
            // 
            this.labelMD.AutoSize = true;
            this.labelMD.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelMD.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelMD.Location = new System.Drawing.Point(50, 351);
            this.labelMD.Name = "labelMD";
            this.labelMD.Size = new System.Drawing.Size(166, 28);
            this.labelMD.TabIndex = 7;
            this.labelMD.Text = "Monitor Duration";
            // 
            // textMD
            // 
            this.textMD.Enabled = false;
            this.textMD.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textMD.Location = new System.Drawing.Point(222, 348);
            this.textMD.Name = "textMD";
            this.textMD.Size = new System.Drawing.Size(73, 34);
            this.textMD.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(301, 351);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 28);
            this.label1.TabIndex = 9;
            this.label1.Text = "[sec]";
            // 
            // checkBoxTM
            // 
            this.checkBoxTM.AutoSize = true;
            this.checkBoxTM.Enabled = false;
            this.checkBoxTM.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.checkBoxTM.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.checkBoxTM.Location = new System.Drawing.Point(418, 350);
            this.checkBoxTM.Name = "checkBoxTM";
            this.checkBoxTM.Size = new System.Drawing.Size(212, 32);
            this.checkBoxTM.TabIndex = 10;
            this.checkBoxTM.Text = "Transparent Monitor";
            this.checkBoxTM.UseVisualStyleBackColor = true;
            // 
            // ActionFrom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.checkBoxTM);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textMD);
            this.Controls.Add(this.labelMD);
            this.Controls.Add(this.butSendCMD);
            this.Controls.Add(this.comboBoxCMD);
            this.Controls.Add(this.labelCMD);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.textBoxSys);
            this.Controls.Add(this.labelSys);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ActionFrom";
            this.Text = "Action";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelSys;
        private System.Windows.Forms.TextBox textBoxSys;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label labelCMD;
        private System.Windows.Forms.Button butSendCMD;
        private System.Windows.Forms.Label labelMD;
        private System.Windows.Forms.TextBox textMD;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBoxTM;
        private System.Windows.Forms.ComboBox comboBoxCMD;
    }
}