﻿using DigitalSync.Models;
using DigitalSync.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DigitalSync
{
    class Service
    {
        public delegate void OnMessage(RadioData data);
        public delegate void OnConnected();
        public delegate void OnDisconnected();

        public event OnMessage MessageListening;
        public event OnConnected ConnectedListening;
        public event OnDisconnected DisconnectedListening;

        internal static int AUDIO_PORT = 5060;
        private static int BROADCAST_PORT = 9760;

        private static int CMD_PORT = 974;

        private static string ADDRESS = "127.0.0.1";

        private static string AUDIO_ADDRESS = "127.0.0.1";

        private Socket m_sock;
        private byte[] m_byBuff = new byte[256]; // Recieved data buffer
        //private byte[] m_byBuff;
        private Thread thread;
        private List<Profile> profile;
        private List<ProfileGroup> profileGroup;

        public string incoming_id;
        private Socket InstanceSocket()
        {
            return (m_sock == null) ? m_sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp) : m_sock;
        }

        public void Destroy()
        {
            try
            {
                var sock = InstanceSocket();
                // Close the socket if it is still open
                if (sock != null && sock.Connected)
                {
                    sock.Shutdown(SocketShutdown.Both);
                    Thread.Sleep(10);
                    sock.Close();
                    sock = null;

                }

                thread.Abort();

            }
            catch (Exception e)
            {
                if (!m_sock.Connected)
                {
                    DisconnectedListening();
                    Console.WriteLine("Unable to connect to remote machine", "Connect Failed!");
                }
            }
        }

        public void StartAsync()
        {
            profile = Profile.GetProfiles();
            profileGroup = ProfileGroup.GetProfileGroup();
            var sock = InstanceSocket();

            if (m_sock.Connected)
            {
                Destroy();
            }

            Thread.Sleep(3 * 1000);
            thread = new Thread(ClientConnect);
            thread.IsBackground = true;
            thread.Start();
        }

        private void ClientConnect()
        {
            try
            {
                m_sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); // Server connection

                ADDRESS = Properties.Settings.Default.SocketURL;

                // Define the Server address and port
                IPEndPoint epServer = new IPEndPoint(IPAddress.Parse(ADDRESS), BROADCAST_PORT);

                // Connect to the server blocking method and setup callback for recieved data
                // m_sock.Connect( epServer );
                // SetupRecieveCallback( m_sock );

                // Connect to server non-Blocking method
                m_sock.Blocking = false;
                AsyncCallback onconnect = new AsyncCallback(OnConnect);
                m_sock.BeginConnect(epServer, onconnect, m_sock);

            }
            catch (Exception e)
            {
                e.ToString();
            }
        }

        public void OnConnect(IAsyncResult ar)
        {
            // Socket was the passed in object
            Socket sock = (Socket)ar.AsyncState;

            // Check if we were sucessfull
            try
            {
                //sock.EndConnect( ar );
                if (sock.Connected)
                {
                    ConnectedListening();
                    SetupRecieveCallback(sock);
                }
                else
                {
                    if (!m_sock.Connected)
                    {
                        DisconnectedListening();
                        Console.WriteLine("Unable to connect to remote machine", "Connect Failed!");
                    }
                }
            }
            catch (Exception ex)
            {
                if (!m_sock.Connected)
                {
                    DisconnectedListening();
                    Console.WriteLine("Unable to connect to remote machine", "Connect Failed!");
                }
            }
        }

        /// <summary>
        /// Get the new data and send it out to all other connections. 
        /// Note: If not data was recieved the connection has probably 
        /// died.
        /// </summary>
        /// <param name="ar"></param>
        public void OnClientRecievedData(IAsyncResult ar)
        {
            // Socket was the passed in object
            Socket sock = (Socket)ar.AsyncState;

            // Check if we got any data
            try
            {
                int nBytesRec = sock.EndReceive(ar);
                if (nBytesRec > 0)
                {
                    // Wrote the data to the List
                    string sRecieved = Encoding.ASCII.GetString(m_byBuff, 0, nBytesRec);
                    //Console.WriteLine("receive data : " + sRecieved);
                    int byteLength = Buffer.ByteLength(m_byBuff);
                    Debug.WriteLine($"m_byBuff length :{byteLength} " );
                    Debug.WriteLine($"m_byBuff :{sRecieved} ");
                    //PrintByteArray(m_byBuff);
                    try
                    {

                        if (sRecieved.Substring(1, 4) == "gB1U")
                        {
                            Debug.WriteLine($"Incomming from:{sRecieved.Substring(4, 6)}");
                            int id_get = int.Parse(sRecieved.Substring(5, 5));
                            Debug.WriteLine($"GET ID:{id_get}");
                            //Properties.Settings.Default.UID = id_get;
                            incoming_id = id_get.ToString();
                            // Properties.Settings.Default.profile_select = profile.Find(obj => obj.UID == id_get).Id;
                            // Properties.Settings.Default.Save();
                        } else if (sRecieved.Substring(1, 4) == "xB1U") 
                        {
                            Debug.WriteLine($"Incomming from:{sRecieved.Substring(4, 9)}");
                            int id_get = int.Parse(sRecieved.Substring(5, 8));
                            Debug.WriteLine($"GET ID:{id_get}");
                            incoming_id = id_get.ToString();
                        }
                        //JObject obj = JsonConvert.DeserializeObject<JObject>(sRecieved);

                        //if (obj["name"].ToString() == NameType.GPS.ToString())
                        //{
                        //    MessageListening(new RadioData(obj["name"].ToString(), obj["action"].ToString(), obj["value"].ToString()));
                        //}
                        //else
                        //{
                        //    RadioData radioData = JsonConvert.DeserializeObject<RadioData>(sRecieved);
                        //    MessageListening(radioData);
                        //}

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                    }

                    // WARNING : The following line is NOT thread safe. Invoke is
                    // m_lbRecievedData.Items.Add( sRecieved );
                    //Invoke(m_AddMessage, new string[] { sRecieved });

                    // If the connection is still usable restablish the callback
                    SetupRecieveCallback(sock);
                }
                else
                {
                    // If no data was recieved then the connection is probably dead
                    Console.WriteLine("Client {0}, disconnected", sock.RemoteEndPoint);
                    sock.Shutdown(SocketShutdown.Both);
                    sock.Close();
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(this, ex.Message, "Unusual error druing Recieve!");
            }
        }

        /// <summary>
        /// Setup the callback for recieved data and loss of conneciton
        /// </summary>
        public void SetupRecieveCallback(Socket sock)
        {
            try
            {
                AsyncCallback recieveData = new AsyncCallback(OnClientRecievedData);
                sock.BeginReceive(m_byBuff, 0, m_byBuff.Length, SocketFlags.None, recieveData, sock);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(this, ex.Message, "Setup Recieve Callback failed!");
            }
        }

        public void SocketSendData(string data)
        {
            // Read the message from the text box and send it
            try
            {
                // Check we are connected
                if (m_sock == null || !m_sock.Connected)
                {
                    //MessageBox.Show(this, "Must be connected to Send a message");
                    return;
                }

                // Convert to byte array and send.
                Byte[] byteDateLine = Encoding.ASCII.GetBytes(data.ToCharArray());
                m_sock.Send(byteDateLine, byteDateLine.Length, 0);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(this, ex.Message, "Send Message Failed!");
            }
        }

        public void SocketSendData(RadioData data)
        {
            // Read the message from the text box and send it
            try
            {
                // Check we are connected
                if (m_sock == null || !m_sock.Connected)
                {
                    //MessageBox.Show(this, "Must be connected to Send a message");
                    return;
                }

                var json = JsonConvert.SerializeObject(data) + "\n";

                // Convert to byte array and send.
                Byte[] byteDateLine = Encoding.ASCII.GetBytes(json.ToCharArray());
                m_sock.Send(byteDateLine, byteDateLine.Length, 0);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(this, ex.Message, "Send Message Failed!");
            }
        }

        public void SocketSendData(GPS gps)
        {
            // Read the message from the text box and send it
            try
            {
                // Check we are connected
                if (m_sock == null || !m_sock.Connected)
                {
                    //MessageBox.Show(this, "Must be connected to Send a message");
                    return;
                }

                var json = JsonConvert.SerializeObject(gps) + "\n";

                // Convert to byte array and send.
                Byte[] byteDateLine = Encoding.ASCII.GetBytes(json.ToCharArray());
                m_sock.Send(byteDateLine, byteDateLine.Length, 0);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(this, ex.Message, "Send Message Failed!");
            }
        }

        public void SocketSendData(NXDN nxdn_cmd) 
        {
            Byte[] byteDateLine = { 0x02, 0x41, 0x30, 0x31, 0x03 };
            m_sock.Send(byteDateLine, byteDateLine.Length, 0);
        }
        public void SocketSendData(Byte[] data) 
        {
            m_sock.Send(data, data.Length, 0);
        }

        public void PrintByteArray(byte[] bytes)
        {
            var sb = new StringBuilder("new byte[] { ");
            foreach (var b in bytes)
            {
                sb.Append(b + ", ");
            }
            sb.Append("}");
            Debug.WriteLine(sb.ToString());
        }
    }
}
