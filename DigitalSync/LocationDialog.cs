﻿using DigitalSync.Models;
using DigitalSync.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DigitalSync
{
    public partial class LocationDialog : Form
    {
        static LocationDialog MsgBox;
        static DialogResult result = DialogResult.No;

        static MainForm mainform;

        public LocationDialog()
        {
            InitializeComponent();
        }

        public static DialogResult MSGShow(MainForm mainForm)
        {
            mainform = mainForm;
            MsgBox = new LocationDialog();
            MsgBox.ShowDialog();
            return result;
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            result = DialogResult.OK;

            if(textBoxUID.Text.ToString().Trim() != "")
            {
                if (mainform.Service != null)
                {
                    List<Device> gps = new List<Device>();
                    gps.Add(new Device(textBoxUID.Text.ToString()));
                    mainform.Service.SocketSendData(
                            new GPS(
                                NameType.GPS.ToString(), ActionType.get.ToString(),
                                gps
                            )
                    );
                }
            }

            MsgBox.Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            result = DialogResult.No;
            MsgBox.Close();
        }
    }
}
