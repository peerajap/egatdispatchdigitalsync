﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigitalSync.Models
{
    class Device
    {
        private string ID;
        private string LOCATION;
        private GmapDevice gmapDevice;

        public Device(string iD)
        {
            ID = iD;
        }

        public Device(string iD, string lOCATION)
        {
            ID = iD;
            LOCATION = lOCATION;
        }

        public string id { get => ID; set => ID = value; }
        public string location { get => LOCATION; set => LOCATION = value; }
        public GmapDevice GmapDevice { get => gmapDevice; set => gmapDevice = value; }
    }
}
