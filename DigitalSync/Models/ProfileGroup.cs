﻿using System;
using System.Collections.Generic;
using System.Text;
using DigitalSync.DB;
using SQLite;

namespace DigitalSync.Models
{
    public class ProfileGroup
    {
        [PrimaryKey]
        public int Id { get; set; }
        public int Group { get; set; }
        public string Name { get; set; }
        public ProfileGroup(int id, int group, string name)
        {
            this.Id = id;
            this.Group = group;
            this.Name = name;
        }

        public ProfileGroup() { }
        public static List<ProfileGroup> GetProfileGroup()
        {
            List<ProfileGroup> profiles = new List<ProfileGroup>();
            try
            {
                var db = DBProvider.CreateConnection();
                db = DBProvider.CreateConnectionNew();
                var p = db.Table<ProfileGroup>().ToList();
                foreach (ProfileGroup profile in p)
                    profiles.Add(profile);

                db.Close();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return profiles;
        }

        public static void Update(ProfileGroup profile)
        {
            var db = DBProvider.CreateConnection();
            db = DBProvider.CreateConnectionNew();
            var result = db.Update(profile);
            db.Close();
        }
    }
}
