﻿using GMap.NET;
using System;
using System.Collections.Generic;
using System.Text;
using GMap.NET.WindowsForms;
using System.Runtime.Serialization;
using System.Drawing;

namespace DigitalSync.Models
{
    [Serializable]
    class GmapDevice : GMapMarker, ISerializable
    {
        private int Radius;
        private Device deviceObj;

        public GmapDevice(
            PointLatLng p, Device device) : base(p)
        {
            Radius = 1888; // 888m
            deviceObj = device;
            IsHitTestVisible = false;
        }

        public override void OnRender(Graphics g)
        {
            try
            {
                // Draw image to screen.
                g.DrawImage(LoadNewPict(), LocalPosition.X - 25, LocalPosition.Y - 25, 50, 50);

                // Draw Text
                g.DrawString(deviceObj.id, new Font("Segoe UI", 10F, FontStyle.Bold, GraphicsUnit.Point),
                    new SolidBrush(Color.FromArgb(200, 0, 0)), LocalPosition.X - deviceObj.id.Length * 5.5F, LocalPosition.Y - 45,
                    new StringFormat());

            }
            catch (Exception e)
            {
                e.ToString();
            }

        }

        private Image LoadNewPict()
        {
            // You should replace the bold image   
            // in the sample below with an icon of your own choosing.  
            // Note the escape character used (@) when specifying the path.  
            return Properties.Resources.phone_icon;
        }

        public override void Dispose()
        {
            base.Dispose();
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            // TODO: Radius, IsFilled
        }

        protected GmapDevice(SerializationInfo info, StreamingContext context)
           : base(info, context)
        {
            // TODO: Radius, IsFilled
        }
    }
}
