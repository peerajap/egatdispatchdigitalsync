﻿using System;
using System.Collections.Generic;
using System.Text;
using DigitalSync.DB;

namespace DigitalSync.Models
{
    class NXDN
    {
        private string NAME;
        private string ACTION;
        private List<Device> VALUE;

        public NXDN(string nAME, string aCTION)
        {
            NAME = nAME;
            ACTION = aCTION;
        }

        public string name { get => NAME; set => NAME = value; }
        public string action { get => ACTION; set => ACTION = value; }
        public List<Device> value { get => VALUE; set => VALUE = value; }
    }
}
