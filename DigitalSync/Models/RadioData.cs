﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSync.Models
{
    class RadioData
    {
        private string NAME;
        private string ACTION;
        private string VALUE;

        public RadioData(string name, string action, string value)
        {
            this.NAME = name;
            this.ACTION = action;
            this.VALUE = value;
        }

        public string name { get => NAME; set => NAME = value; }
        public string action { get => ACTION; set => ACTION = value; }
        public string value { get => VALUE; set => this.VALUE = value; }
    }
}
