﻿using DigitalSync.Models;
using DigitalSync.Utils;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace DigitalSync
{
    public partial class MainForm : Form
    {
        public MapUserControl mapUserControl;
        public PTTUserControl pttUserControl;

        public WebMap webMap;

        private Service service;
        public string Mode = "UID";
        public string Readio_Mode = "NXDN";
        Timer _timer = null;

        internal Service Service { get => service; }

        public MainForm()
        {
            InitializeComponent();
            InitializeUserControl();
            InitializeService();
            InitTimer();
            this.Readio_Mode = "DMR";

        }

        private void InitTimer()
        {
            if (_timer == null)
            {
                _timer = new Timer();
                _timer.Interval = 1000;
                _timer.Tick += _timer_Tick;
            }
            _timer.Start();
        }

        private void InitializeService()
        {
            service = new Service();
            try
            {
                service.DisconnectedListening += () =>
                {
                    pttUserControl.OnDisonnect();
                };
                service.ConnectedListening += () =>
                {
                    pttUserControl.OnConnected();
                };
                service.MessageListening += (data) =>
                {
                    if(data.name == NameType.GPS.ToString())
                    {
                        mapUserControl.OnMessage(data);
                    }
                    else
                    {
                        pttUserControl.OnMessage(data);
                    }
                };

            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            service.StartAsync();
        }

        private void InitializeUserControl()
        {
            mapUserControl = new MapUserControl(this);
            pttUserControl = new PTTUserControl(this);
            webMap = new WebMap(this);
            
            panel_gmap.Controls.Add(webMap);
            center_right.Controls.Add(pttUserControl);

        }

        void _timer_Tick(object sender, EventArgs e)
        {
            UpdateDigitalClock();
            if (pttUserControl != null)
            {
                pttUserControl._timer_Tick(sender, e);
            }
            Console.WriteLine("{0} Bps", "");
        }

        private void DTMF_Click(object sender, EventArgs e)
        {

        }

        private void UpdateDigitalClock()
        {
            string formatTime = "HH:mm:ss";
            string formatDate = "dd MMMM yyyy";
            DateTime now = DateTime.Now;
            this.labelTime.Text = now.ToString(formatTime);
            this.labelDate.Text = now.ToString(formatDate);
        }

        private void center_right_Resize(object sender, EventArgs e)
        {
            if (pttUserControl != null)
            {
                pttUserControl.Width = this.center_right.Width;
                pttUserControl.Height = this.center_right.Height;

                pttUserControl.KeyDown += new KeyEventHandler(pttUserControl.PTTButton_KeyDown);
                pttUserControl.KeyUp += new KeyEventHandler(pttUserControl.PTTButton_KeyUp);
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            pttUserControl.OnDestroy();
            pttUserControl.Dispose();
            mapUserControl.Dispose();
            service.Destroy();
            _timer.Stop();
        }

        private void Settings_Click(object sender, EventArgs e)
        {
            pttUserControl.KeyDown += new KeyEventHandler(pttUserControl.PTTButton_KeyDown);
            pttUserControl.KeyUp += new KeyEventHandler(pttUserControl.PTTButton_KeyUp);

            DialogResult result = Settings.MSGShow();
            try
            {
                if (result == DialogResult.OK)
                {
                    service.StartAsync();

                    pttUserControl.OnDestroy();
                    pttUserControl.StartAsync();

                }else if (result == DialogResult.No)
                {
                    service.Destroy();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        private void location_Click(object sender, EventArgs e)
        {
            //DialogResult result = LocationDialog.MSGShow(this);

            //pttUserControl.KeyDown += new KeyEventHandler(pttUserControl.PTTButton_KeyDown);
            //pttUserControl.KeyUp += new KeyEventHandler(pttUserControl.PTTButton_KeyUp);

            if (this.Readio_Mode == "NXDN")
            {
                string UID = Properties.Settings.Default.UID.ToString();
                string UID_ADD0 = new string('0', 5 - UID.Length);
                byte[] bytes_uid = Encoding.ASCII.GetBytes(UID_ADD0 + UID);
                byte[] gps_reuest = { 0x02, 0x67, 0x52, 0x33, 0x55, bytes_uid[0], bytes_uid[1],
                                        bytes_uid[2],bytes_uid[3],bytes_uid[4], 0x31 ,0x30 ,0x03 };
                Service.SocketSendData(gps_reuest);
                Debug.WriteLine($"{gps_reuest}");
            }
            else 
            {
                string UID = Properties.Settings.Default.UID.ToString();
                string UID_ADD0 = new string('0', 8 - UID.Length);
                byte[] bytes_uid = Encoding.ASCII.GetBytes(UID_ADD0 + UID);
                byte[] gps_reuest = { 0x02, 0x78, 0x52, 0x33, 0x55, bytes_uid[0], bytes_uid[1],
                                        bytes_uid[2],bytes_uid[3],bytes_uid[4],bytes_uid[5],
                                        bytes_uid[6],bytes_uid[7], 0x31 ,0x30 ,0x03 };
                Service.SocketSendData(gps_reuest);
                Debug.WriteLine($"{gps_reuest}");
            }
            System.Threading.Thread.Sleep(4000);
            webMap.Reinit();
            // webMap.go_to_gooole();

        }

        private void History_Click(object sender, EventArgs e)
        {
            HistoryForm.MSGShow();
        }
        private int NNN = 0;
        private void GroupCall_Click(object sender, EventArgs e)
        {
            // Console.WriteLine("INDIVIDUAL");
            Debug.WriteLine($"GroupCall_Click{NNN}");
            this.Mode = "Group";
            Debug.WriteLine($"Mode = {this.Mode}");
            pttUserControl.ReInitData();
            pttUserControl.ReInitListButtonChannelControl();
            pttUserControl.ChangeRecordFunction2();
            //pttUserControl.Refresh();
            //this.Refresh();
            NNN++;
        }

        private void Action_Click(object sender, EventArgs e)
        {
            ActionFrom actionFrom = new ActionFrom(this);
            //actionFrom.RadioSPC_CMD()
            actionFrom.ShowDialog();

        }

        private void INDIVIDUAL_Click(object sender, EventArgs e)
        {
            this.Mode = "UID";
            //InitializeUserControl();
            Debug.WriteLine($"Mode = {this.Mode}");
            pttUserControl.ReInitData();
            pttUserControl.ReInitListButtonChannelControl();
            pttUserControl.ChangeRecordFunction2();
            //pttUserControl.Refresh();
            //this.Refresh();
        }

    }
}
