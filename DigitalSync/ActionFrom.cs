﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DigitalSync.DB;
using DigitalSync.Models;


namespace DigitalSync
{
    public partial class ActionFrom : Form
    {
        private MainForm mainForm;
        private bool UID_MODE = true;
        private byte[] Device_ID = { 0, 0, 0, 0, 0 };
        private byte[,] cmd = new byte[,] { };
        public ActionFrom(MainForm mainForm)
        {
            InitializeComponent();
            this.checkBoxTM.Text = "";
            this.checkBoxTM.Visible = false;
            this.mainForm = mainForm;
            cmd = new byte[,]
            {
                {0x02,0x67,0x3C,0x34,Device_ID[0], Device_ID[1],Device_ID[2],Device_ID[3],Device_ID[4],0x30,0x31,0x03},
                {0x02,0x67,0x3C,0x35,Device_ID[0], Device_ID[1],Device_ID[2],Device_ID[3],Device_ID[4],0x30,0x31,0x03},
                {0x02,0x67,0x3C,0x36,Device_ID[0], Device_ID[1],Device_ID[2],Device_ID[3],Device_ID[4],0x30,0x31,0x03},
                
            };
            if (this.mainForm.Mode == "UID")
            {
                Debug.WriteLine("Action UID");
                UID_MODE = true;
            }
            else
            {
                Debug.WriteLine("Action GROUP");
                UID_MODE = false;
            }
            // GetDB();
            Acive_ID();
        }

        public void RadioSPC_CMD()
        {
            if (mainForm.Mode == "UID" && mainForm.Readio_Mode == "NXDN")
            {
                string UID = Properties.Settings.Default.UID.ToString();
                string UID_ADD0 = new string('0', 5 - UID.Length);
                // Debug.WriteLine($"Add 0 : {UID_ADD0}");
                Debug.WriteLine($"UID Send:{UID_ADD0 + UID}");
                Device_ID = Encoding.ASCII.GetBytes(UID_ADD0 + UID);
                // mainForm.Service.SocketSendData(cmd[comboBoxCMD.SelectedIndex]);
                if (comboBoxCMD.SelectedIndex == 0)
                {
                    byte[] cmd_stun =  { 0x02,0x67,0x3C,0x34,0x55,Device_ID[0], Device_ID[1],Device_ID[2],
                        Device_ID[3],Device_ID[4],0x30,0x31,0x03};
                    mainForm.Service.SocketSendData(cmd_stun);
                }
                else if (comboBoxCMD.SelectedIndex == 1)
                {
                    byte[] cmd_revive = { 0x02, 0x67, 0x3C, 0x35,0x55, Device_ID[0], Device_ID[1], 
                        Device_ID[2], Device_ID[3], Device_ID[4], 0x30, 0x31, 0x03 };
                    mainForm.Service.SocketSendData(cmd_revive);

                }
                else if (comboBoxCMD.SelectedIndex == 2)
                {
                    byte[] cmd_kill = { 0x02, 0x67, 0x3C, 0x36,0x55, Device_ID[0], Device_ID[1], 
                        Device_ID[2], Device_ID[3], Device_ID[4], 0x30, 0x31, 0x03 };
                    mainForm.Service.SocketSendData(cmd_kill);
                }
                else if (comboBoxCMD.SelectedIndex == 3) 
                {
                    byte Silent;
                    if (checkBoxTM.Checked)
                    {
                        Silent = 0x53;
                    }
                    else 
                    {
                        Silent = 0x57;
                    }
                    string time_s = String.Format("{0:D3}", Convert.ToInt32(textMD.Text));
                    byte[] time = Encoding.ASCII.GetBytes(time_s);
                    byte[] cmd_monitor = { 0x02, 0x67, 0x3C, 0x30,0x55, Device_ID[0], Device_ID[1], 
                        Device_ID[2],Device_ID[3], Device_ID[4], Silent, time[0], time[1], time[2], 
                        0x30, 0x31, 0x03 };
                    mainForm.Service.SocketSendData(cmd_monitor);
                }
            }
            else if (mainForm.Mode == "UID" && mainForm.Readio_Mode == "DMR")
            {
                string UID = Properties.Settings.Default.UID.ToString();
                string UID_ADD0 = new string('0', 8 - UID.Length);
                // Debug.WriteLine($"Add 0 : {UID_ADD0}");
                Debug.WriteLine($"UID Send:{UID_ADD0 + UID}");
                Device_ID = Encoding.ASCII.GetBytes(UID_ADD0 + UID);
                if (comboBoxCMD.SelectedIndex == 0)
                {
                    byte[] cmd_stun =  { 0x02,0x78,0x3C,0x34,0x55,Device_ID[0], Device_ID[1],Device_ID[2],
                        Device_ID[3],Device_ID[4],Device_ID[5],Device_ID[6],Device_ID[7],0x30,0x31,0x03};
                    mainForm.Service.SocketSendData(cmd_stun);
                }
                else if (comboBoxCMD.SelectedIndex == 1)
                {
                    byte[] cmd_revive = { 0x02, 0x78, 0x3C, 0x35,0x55, Device_ID[0], Device_ID[1],
                        Device_ID[2], Device_ID[3], Device_ID[4],Device_ID[5],Device_ID[6],Device_ID[7]
                        , 0x30, 0x31, 0x03 };
                    mainForm.Service.SocketSendData(cmd_revive);

                }
                else if (comboBoxCMD.SelectedIndex == 2)
                {
                    byte[] cmd_kill = { 0x02, 0x78, 0x3C, 0x36,0x55, Device_ID[0], Device_ID[1],
                        Device_ID[2], Device_ID[3], Device_ID[4],Device_ID[5],Device_ID[6],Device_ID[7],
                        0x30, 0x31, 0x03 };
                    mainForm.Service.SocketSendData(cmd_kill);
                }
                else if (comboBoxCMD.SelectedIndex == 3)
                {
                    byte Silent;
                    if (checkBoxTM.Checked)
                    {
                        Silent = 0x53;
                    }
                    else
                    {
                        Silent = 0x57;
                    }
                    string time_s = String.Format("{0:D3}", Convert.ToInt32(textMD.Text));
                    byte[] time = Encoding.ASCII.GetBytes(time_s);
                    byte[] cmd_monitor = { 0x02, 0x78, 0x3C, 0x30,0x55,Device_ID[0], Device_ID[1],
                        Device_ID[2], Device_ID[3], Device_ID[4],Device_ID[5],Device_ID[6],Device_ID[7], 
                        Silent, time[0], time[1], time[2],
                        0x30, 0x31, 0x03 };
                    mainForm.Service.SocketSendData(cmd_monitor);
                }
            }
        }
        private void Acive_ID()
        {
            this.textBoxName.Text = Properties.Settings.Default.NAME;
            if (UID_MODE)
            {
                this.textBoxSys.Text = "UID:" + Properties.Settings.Default.UID.ToString();
            }
            else
            {
                this.textBoxSys.Text = "G:" + Properties.Settings.Default.GROUPID.ToString();
            }
        }
        private void GetDB()
        {
            var db = DBProvider.CreateConnection();
            db = DBProvider.CreateConnectionNew();
            var p = db.Table<Profile>().ToList();
            //Debug.WriteLine($"ID: {p[0].Id}");
            //Debug.WriteLine($"ID: {p[0].Group}");
            foreach (var n in p)
            {
                Debug.WriteLine($"ID: {n.Id}");
                Debug.WriteLine($"Group: {n.Group}");
                Debug.WriteLine($"Name: {n.Name}");
                Debug.WriteLine($"Remark: {n.Remark}"); ;
            }
        }

        private void comboBoxCMD_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxCMD.SelectedIndex == 3)
            {
                this.textMD.Enabled = true;
                this.checkBoxTM.Enabled = true;
                this.checkBoxTM.Text = "Transparent Monitor";
                this.checkBoxTM.Visible = true;
            }
            else 
            {
                this.textMD.Enabled = false;
                this.checkBoxTM.Enabled = false;
                this.checkBoxTM.Text = "";
                this.checkBoxTM.Visible = false;
            }
        }

        private void butSendCMD_Click(object sender, EventArgs e)
        {
            RadioSPC_CMD();
        }
    }
}
