﻿using DigitalSync.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DigitalSync
{
    public partial class HistoryForm : Form
    {
        static HistoryForm historyForm;

        public HistoryForm()
        {
            InitializeComponent();
            InitialData();
        }

        private void InitialData()
        {
            try
            {
                List<HistoryCall> historyCalls = HistoryCall.GetHistoryCall();
                for (int hi = 0; hi < historyCalls.Count; hi++)
                {
                    HistoryCall h = historyCalls[hi];
                    dataGridView1.Rows.Add(h.Datetime.ToString(), "ZONE" + h.Zone, "CH" + h.Channel, h.Uid);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        public static void MSGShow()
        {
            historyForm = new HistoryForm();
            historyForm.ShowDialog();
        }
    }
}
