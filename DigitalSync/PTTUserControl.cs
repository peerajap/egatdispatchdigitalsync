﻿using DigitalSync.Models;
using DigitalSync.Utils;
using NAudio.CoreAudioApi;
using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DigitalSync
{
    public partial class PTTUserControl : UserControl
    {
        private int func_record = 1;
        private bool isDataIncomming;
        private bool isSocketConnected;
        private bool isPTT = false;
        private List<RecordUserControl> recordPanels = new List<RecordUserControl>();
        private List<Profile> profiles;
        private List<ProfileGroup> profilesGroup;

        // Audio Recording
        int Counter = 0;
        WaveIn _waveIn;
        int sampleRate = 44100;
        byte[] _notEncodedBuffer = new byte[0];

        // Volume
        private MMDevice device;

        private UdpClient udpClient;

        Thread thread_connect;
        Thread thread_listening;
        private static int AUDIO_PORT = 5060;

        private MainForm mainForm;
        private int pttCounter;

        public PTTUserControl(MainForm mainForm)
        {
            this.mainForm = mainForm;
            InitializeComponent();
            InitData();
            InitListButtonChannelControl();

            StartAsync();

            var deviceEnumerator = new MMDeviceEnumerator();
            device = deviceEnumerator.GetDefaultAudioEndpoint(DataFlow.Render, Role.Multimedia);

        }

        private void PTTUserControl_Load(object sender, EventArgs e)
        {
            device.AudioEndpointVolume.OnVolumeNotification += AudioEndpointVolume_OnVolumeNotification;
            this.KeyDown += new KeyEventHandler(this.PTTButton_KeyDown);
            this.KeyUp += new KeyEventHandler(this.PTTButton_KeyUp);

            UpdateVolume();
            UpdateMuted();
        }

        void AudioEndpointVolume_OnVolumeNotification(AudioVolumeNotificationData data)
        {
            try
            {
                this.Invoke(new Action(delegate ()
                {
                    UpdateVolume();
                    UpdateMuted();
                }));
            }
            catch
            {
            }
        }

        public void UpdateVolume()
        {
            try
            {
                Image[] level_imgs = {
                    null, Properties.Resources.volume_level_1, Properties.Resources.volume_level_2,
                    Properties.Resources.volume_level_3, Properties.Resources.volume_level_4, Properties.Resources.volume_level_5,
                    Properties.Resources.volume_level_6, Properties.Resources.volume_level_7, Properties.Resources.volume_level_8,
                    Properties.Resources.volume_level_9, Properties.Resources.volume_level_10
                };

                int volume = (int)(Math.Round(device.AudioEndpointVolume.MasterVolumeLevelScalar * 100));
                int level = (volume > 0 && volume < 10) ? 1 : volume / 10;

                this.volumeLevel.BackgroundImage = level_imgs[level];
                this.Speaker.BackgroundImage = (level >= 7) ? Properties.Resources.speaker_full : Properties.Resources.speaker_medium;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        public void UpdateMuted()
        {
            try
            {
                bool mute;
                mute = device.AudioEndpointVolume.Mute;
                if (mute)
                {
                    this.Speaker.BackgroundImage = Properties.Resources.speaker_mute;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        private void InitListButtonChannelControl()
        {
            Debug.WriteLine("Add button");
            if (mainForm.Mode == "UID") 
            {
                profiles = Profile.GetProfiles();
                Debug.WriteLine($"# of profiles = {profiles.Count()}");
                for (int ri = 0; ri < profiles.Count() / 3; ri++)
                {
                    List<ButtonFuncUserControl> buttons = new List<ButtonFuncUserControl>();
                    for (int bi = 0; bi < 3; bi++)
                    {
                        int pi = ((bi + 1) + (ri * 3)) - 1;
                        ButtonFuncUserControl buttonFuncUser = new ButtonFuncUserControl(this, profiles[pi]);
                        buttonFuncUser.G.Text = "G" + profiles[pi].Group;
                        buttonFuncUser.FUNCTION.Text = "UID" + profiles[pi].UID.ToString();
                        buttonFuncUser.NAME.Text = profiles[pi].Name;
                        buttonFuncUser.InitTextDisplay();

                        buttonFuncUser.selectListening += () =>
                        {
                            int pSelect = Properties.Settings.Default.profile_select;
                            Profile p = profiles[pSelect];
                            Properties.Settings.Default.GROUPID = p.Group;
                            Properties.Settings.Default.NAME = p.Name;
                            Properties.Settings.Default.Save();

                            InitData();
                            mainForm.Service.SocketSendData(
                               new RadioData(NameType.GROUP.ToString(), ActionType.set.ToString(), "G" + p.Group)
                            );

                        };

                        buttons.Add(buttonFuncUser);
                    }

                    recordPanels.Add(new RecordUserControl(buttons));
                }
            }
            else 
            {
                profilesGroup = ProfileGroup.GetProfileGroup();
                Debug.WriteLine($"# of profilesGroup = {profilesGroup.Count()}");
                for (int ri = 0; ri < profilesGroup.Count() / 3; ri++)
                {
                    List<ButtonFuncUserControl> buttons = new List<ButtonFuncUserControl>();
                    for (int bi = 0; bi < 3; bi++)
                    {
                        int pi = ((bi + 1) + (ri * 3)) - 1;
                        ButtonFuncUserControl buttonFuncUser = new ButtonFuncUserControl(this, profilesGroup[pi]);
                        buttonFuncUser.G.Text = "G" + profilesGroup[pi].Group;
                        buttonFuncUser.FUNCTION.Text = "";
                        buttonFuncUser.NAME.Text = profilesGroup[pi].Name;
                        buttonFuncUser.InitTextDisplay();

                        buttonFuncUser.selectListening += () =>
                        {
                            int pSelect = Properties.Settings.Default.profile_select;
                            ProfileGroup p = profilesGroup[pSelect];
                            Properties.Settings.Default.GROUPID = p.Group;
                            Properties.Settings.Default.NAME = p.Name;
                            Properties.Settings.Default.Save();

                            InitData();
                            mainForm.Service.SocketSendData(
                               new RadioData(NameType.GROUP.ToString(), ActionType.set.ToString(), "G" + p.Group)
                            );

                        };

                        buttons.Add(buttonFuncUser);
                    }

                    recordPanels.Add(new RecordUserControl(buttons));
                }
            }
            
        }
        public void ReInitListButtonChannelControl() 
        {
            InitListButtonChannelControl();
            //recordPanels.
        }
        public void ReInitData() 
        {
            recordPanels.Clear();
            if (mainForm.Mode == "UID")
            {
                this.label1.Text = "UID";
                try
                {
                    this.labelCH_Display.Text = "UID" + Properties.Settings.Default.UID;
                }
                catch (Exception e) 
                {
                    this.labelCH_Display.Text = "UID" + profiles[0].UID.ToString();
                }
            }
            else if (mainForm.Mode == "GROUP") 
            {
                List<ProfileGroup> profiles2 = ProfileGroup.GetProfileGroup();
                this.label1.Text = "G";
                Properties.Settings.Default.GROUPID = profiles2[0].Group;
                this.labelCH_Display.Text = "G" + Properties.Settings.Default.GROUPID;
            }

            this.labelSendUID_Display.Text = Properties.Settings.Default.SENDID;
            this.labelName_Display.Text = Properties.Settings.Default.NAME.Trim();

            InitTextDisplay();

            foreach (var rp in recordPanels)
            {
                foreach (var buttonControl in rp.buttonFuncUserControls)
                {
                    buttonControl.InitTextDisplay();
                    // buttonControl.Refresh();
                }
            }
        }
        internal void OnConnected()
        {
            try
            {
                isSocketConnected = true;
                labelPTTStatus_Display.Invoke(new MethodInvoker(delegate { this.labelPTTStatus_Display.Text = "PTT Standby"; }));
                label_SocketStatus.Invoke(new MethodInvoker(delegate { label_SocketStatus.Text = "Connected"; }));
                pictureBox_SocketStatus.Invoke(new MethodInvoker(delegate { pictureBox_SocketStatus.BackgroundImage = Properties.Resources.connect_machine1; }));

                mainForm.Service.SocketSendData(
                     new RadioData(NameType.GROUP.ToString(), ActionType.get.ToString(), "")
                );

                mainForm.Service.SocketSendData(
                     new RadioData(NameType.UID.ToString(), ActionType.get.ToString(), "")
                );
            }
            catch (Exception e)
            {
                e.ToString();
            }

        }

        internal void OnDisonnect()
        {
            try
            {
                isSocketConnected = false;
                labelPTTStatus_Display.Invoke(new MethodInvoker(delegate { labelPTTStatus_Display.Text = "Disconnected"; }));
                label_SocketStatus.Invoke(new MethodInvoker(delegate { label_SocketStatus.Text = "Disconnected"; }));
                pictureBox_SocketStatus.Invoke(new MethodInvoker(delegate { pictureBox_SocketStatus.BackgroundImage = Properties.Resources.disconnect_machine; }));
            }
            catch (Exception e)
            {
                e.ToString();
            }
        }

        internal void _timer_Tick(object sender, EventArgs e)
        {
            if (!isDataIncomming && !isPTT)
            {
                Counter = 0;
                string formatTime = "HH:mm tt";
                DateTime now = DateTime.Now;

                this.Invoke(new MethodInvoker(delegate
                {
                    this.labelTime_Display.Text = now.ToString(formatTime);
                    this.labelPTTStatus_Display.ForeColor = Color.Gray;
                    this.labelPTTStatus_Display.Text = (this.isSocketConnected) ? "PTT Standby" : "Disconnected";
                }));

                PTTStyleChange(NameType.TERMINATED);

            }
            else
            {
                Counter++;
                this.Invoke(new MethodInvoker(delegate
                {
                    this.labelTime_Display.Text = ((Counter / 60) >= 10 ? (Counter / 60).ToString() : "0" + (Counter / 60)) + ":" + ((Counter % 60) >= 10 ? (Counter % 60).ToString() : "0" + (Counter % 60));
                    this.labelPTTStatus_Display.Text = (isDataIncomming) ? "Receiving" + "From:U" + mainForm.Service.incoming_id : "Transmitting";
                    this.labelPTTStatus_Display.ForeColor = (isDataIncomming) ? Color.Red : Color.Green;
                }));
            }

            isDataIncomming = false;
            InitTextDisplay();

        }

        internal void OnAudioIn(RadioData data)
        {
            PTTStyleChange(NameType.INCOMING);
        }

        internal void OnMessage(RadioData data)
        {
            try
            {
                if (data.name == NameType.GROUP.ToString() && data.action == ActionType.set.ToString())
                {
                    int value = Int32.Parse(data.value.Split("G")[1]);
                    Properties.Settings.Default.CHANNELID = value;
                }
                else if (data.name == NameType.UID.ToString() && data.action == ActionType.set.ToString())
                {
                    //string value = data.value.Split("UID")[1];
                    //Properties.Settings.Default.UID = value;
                }
                else if (data.name == NameType.INCOMING.ToString() && data.action == ActionType.set.ToString())
                {
                    // Save History
                    string value = (data.value == "") ? "Unknown" : data.value;
                    HistoryCall h = new HistoryCall();
                    h.Zone = Properties.Settings.Default.ZONEID;
                    h.Channel = Properties.Settings.Default.CHANNELID;
                    h.Uid = value;
                    h.Datetime = DateTime.Now;

                    HistoryCall.Insert(h);
                }

                Properties.Settings.Default.Save();

                this.Invoke(new MethodInvoker(delegate { InitData(); }));

            }
            catch (Exception e)
            {
                e.ToString();
            }
        }

        private void InitData()
        {
            // this.labelUUID.Text = Properties.Settings.Default.UID;
            List<Profile> profiles2 = Profile.GetProfiles();
            
            if (mainForm.Mode == "UID")
            {
                this.label1.Text = "UID";
                try
                {
                    this.labelCH_Display.Text = "UID" + Properties.Settings.Default.UID;
                }
                catch (Exception e) 
                {
                    this.labelCH_Display.Text = "UID" + profiles2[0].UID.ToString();
                }
            }
            else if (mainForm.Mode == "GROUP") 
            {
                this.label1.Text = "G";
                this.labelCH_Display.Text = "G" + Properties.Settings.Default.GROUPID;
            }
            
            this.labelSendUID_Display.Text = Properties.Settings.Default.SENDID;
            this.labelName_Display.Text = Properties.Settings.Default.NAME.Trim();

            InitTextDisplay();

            foreach (var rp in recordPanels)
            {
                foreach (var buttonControl in rp.buttonFuncUserControls)
                {
                    buttonControl.InitTextDisplay();
                }
            }

        }

        private void InitTextDisplay()
        {
            this.labelSendUID_Display.Parent.Padding = new Padding((labelSendUID_Display.Parent.Width - labelSendUID_Display.Width) / 2, 0, 0, 0);
            this.labelName_Display.Parent.Padding = new Padding((labelName_Display.Parent.Width - labelName_Display.Width) / 2, 0, 0, 0);
            this.labelTime_Display.Parent.Padding = new Padding((labelTime_Display.Parent.Width - labelTime_Display.Width) / 2, 0, 0, 0);
            this.labelPTTStatus_Display.Parent.Padding = new Padding((labelPTTStatus_Display.Parent.Width - labelPTTStatus_Display.Width) / 2, 0, 0, 0);
        }

        private void ChangeRecordFunction()
        {
            this.panelFunction.Controls.Clear();
            for (int ri = func_record - 1; ri >= 0; ri--)
            {
                this.panelFunction.Invoke(new MethodInvoker(delegate { this.panelFunction.Controls.Add(recordPanels[ri]); }));
            }
        }

        public void ChangeRecordFunction2() 
        {
            ChangeRecordFunction();
        }

        private void PTTUserControl_SizeChanged(object sender, EventArgs e)
        {
            if (recordPanels.Count() > 0)
            {
                int h_Func = this.panelFunction.Height;
                int h_Record = this.recordPanels[0].Height;

                int record = h_Func / h_Record;
                if (record != func_record)
                {
                    func_record = record;

                    // new object here
                    ChangeRecordFunction();
                    Console.WriteLine("PTT Size change record : " + func_record.ToString());
                }
            }
        }

        public void StartAsync()
        {
            thread_connect = new Thread(ClientConnect);
            thread_listening = new Thread(ClientListening);

            thread_connect.IsBackground = true;
            thread_listening.IsBackground = true;
            thread_connect.Start();
            thread_listening.Start();

        }

        public void OnDestroy()
        {
            try
            {
                OnDisonnect();
                thread_connect.Abort();
                thread_listening.Abort();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        private void ClientConnect()
        {
            try
            {
                string ADDRESS = Properties.Settings.Default.SocketURL;
                ADDRESS = "192.168.8.3";
                //ADDRESS = "127.0.0.1";

                udpClient = new UdpClient();
                udpClient.Connect(ADDRESS, Service.AUDIO_PORT);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private void ClientListening()
        {
            try
            {
                UdpClient udpServer = new UdpClient(Service.AUDIO_PORT);

                WaveOut _waveOut = new WaveOut(WaveCallbackInfo.FunctionCallback());
                BufferedWaveProvider _playBuffer;
                _playBuffer = new BufferedWaveProvider(new WaveFormat(sampleRate, 16, 1));
                _waveOut.Init(_playBuffer);
                _waveOut.Play();

                while (true)
                {
                    var remoteEP = new IPEndPoint(IPAddress.Any, AUDIO_PORT);
                    byte[] buff = udpServer.Receive(ref remoteEP); // listen on port 11000
                    //Debug.WriteLine($"buff len = {buff.Length}");
                    //Debug.WriteLine($"IPAddress.Any = {IPAddress.Any}");
                    try
                    {
                        isDataIncomming = true;
                        _playBuffer.AddSamples(buff, 0, buff.Length);

                        this.Invoke((MethodInvoker)delegate
                        {
                            PTTStyleChange(NameType.INCOMING);

                        });
                    }
                    catch (Exception e)
                    {
                        e.ToString();
                    }

                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
        }

        private void PTTStyleChange(NameType nameType)
        {
            if (nameType == NameType.EndCall || nameType == NameType.TERMINATED)
            {
                // Default
                #region
                this.PTTButton.AlternativeFocusBorderColor = Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(173)))), ((int)(((byte)(240)))));
                this.PTTButton.BackColor = Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(173)))), ((int)(((byte)(240)))));
                this.PTTButton.ForeColor = Color.Lime;
                this.PTTButton.InnerBorderColor = Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(178)))), ((int)(((byte)(248)))));
                this.PTTButton.OuterBorderColor = Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(178)))), ((int)(((byte)(248)))));
                this.PTTButton.ShineColor = Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(178)))), ((int)(((byte)(248)))));
                #endregion
            }
            else if (nameType == NameType.StartCall)
            {
                // PTT
                #region
                this.PTTButton.AlternativeFocusBorderColor = Color.SeaGreen;
                this.PTTButton.BackColor = Color.Green;
                this.PTTButton.ForeColor = Color.White;
                this.PTTButton.InnerBorderColor = Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
                this.PTTButton.OuterBorderColor = Color.Green;
                this.PTTButton.ShineColor = Color.Lime;
                #endregion
            }
            else if (nameType == NameType.INCOMING)
            {
                // Incoming
                #region
                this.PTTButton.AlternativeFocusBorderColor = Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(173)))), ((int)(((byte)(240)))));
                this.PTTButton.BackColor = Color.Red;
                this.PTTButton.ForeColor = Color.White;
                this.PTTButton.InnerBorderColor = Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.PTTButton.OuterBorderColor = Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.PTTButton.ShineColor = Color.Red;
                #endregion
            }
        }

        void StartRecording()
        {
            isPTT = true;
            _waveIn = new WaveIn(WaveCallbackInfo.FunctionCallback());
            _waveIn.BufferMilliseconds = 10;
            _waveIn.DataAvailable += _waveIn_DataAvailable;
            _waveIn.WaveFormat = new WaveFormat(sampleRate, 16, 1);

            _waveIn.StartRecording();

        }

        void StopRecording()
        {
            isPTT = false;
            _waveIn.StopRecording();
            _waveIn.Dispose();
            _waveIn = null;

        }

        void _waveIn_DataAvailable(object sender, WaveInEventArgs e)
        {
            byte[] soundBuffer = new byte[e.BytesRecorded + _notEncodedBuffer.Length];

            for (int i = 0; i < _notEncodedBuffer.Length; i++)
                soundBuffer[i] = _notEncodedBuffer[i];
            for (int i = 0; i < e.BytesRecorded; i++)
                soundBuffer[i + _notEncodedBuffer.Length] = e.Buffer[i];
            try
            {
                udpClient.Send(e.Buffer, e.Buffer.Length);
            }
            catch (Exception send_exception)
            {
                Console.WriteLine(" Exception {0}", send_exception.Message);
            }
        }

        public void PTTDown()
        {
            try
            {
                PTTStyleChange(NameType.StartCall);
                StartRecording();
                // mainForm.Service.SocketSendData(new RadioData(NameType.StartCall.ToString(), ActionType.set.ToString(), labelSendUID_Display.Text.ToString()));
                Debug.WriteLine($"Readio_mode {mainForm.Readio_Mode}");
                if (mainForm.Mode == "UID") 
                {
                    // Debug.WriteLine("UID Mode");
                    // Debug.WriteLine($"{Properties.Settings.Default.UID}");
                    //string UID = Properties.Settings.Default.UID.ToString();
                    //string UID_ADD0 = new string('0', 5-UID.Length);
                    //// Debug.WriteLine($"Add 0 : {UID_ADD0}");
                    //Debug.WriteLine($"UID Send:{UID_ADD0 + UID}");
                    //byte[] bytes_uid = Encoding.ASCII.GetBytes(UID_ADD0 + UID);
                    //byte[] uid_send = { 0x02, 0x67, 0x42, 0x55, bytes_uid[0], bytes_uid[1],
                    //                    bytes_uid[2],bytes_uid[3],bytes_uid[4], 0x31 ,0x30 ,0x03 };
                    //mainForm.Service.SocketSendData(uid_send);
                    if (mainForm.Readio_Mode == "NXDN")
                    {
                        string UID = Properties.Settings.Default.UID.ToString();
                        string UID_ADD0 = new string('0', 5 - UID.Length);
                        // Debug.WriteLine($"Add 0 : {UID_ADD0}");
                        Debug.WriteLine($"UID Send:{UID_ADD0 + UID}");
                        byte[] bytes_uid = Encoding.ASCII.GetBytes(UID_ADD0 + UID);
                        byte[] uid_send = { 0x02, 0x67, 0x42, 0x55, bytes_uid[0], bytes_uid[1],
                                        bytes_uid[2],bytes_uid[3],bytes_uid[4], 0x31 ,0x30 ,0x03 };
                        mainForm.Service.SocketSendData(uid_send);
                    }
                    else 
                    {
                        string DUID = Properties.Settings.Default.UID.ToString();
                        string DUID_ADD0 = new string('0', 8 - DUID.Length);
                        // Debug.WriteLine($"Add 0 : {UID_ADD0}");
                        Debug.WriteLine($"UID Send:{DUID_ADD0 + DUID}");
                        byte[] bytes_uid = Encoding.ASCII.GetBytes(DUID_ADD0 + DUID);
                        byte[] uid_send = { 0x02, 0x78, 0x42, 0x55, bytes_uid[0], bytes_uid[1],
                                        bytes_uid[2],bytes_uid[3],bytes_uid[4],bytes_uid[5],bytes_uid[6]
                                        ,bytes_uid[7], 0x31 ,0x30 ,0x03 };
                        mainForm.Service.SocketSendData(uid_send);
                        Debug.WriteLine($"{Encoding.ASCII.GetString(uid_send)}");
                    }
                }
                else 
                {
                    //// Debug.WriteLine("GROUP Mode");
                    //// Debug.WriteLine($"{Properties.Settings.Default.GROUPID}");
                    //string GROUPID = Properties.Settings.Default.GROUPID.ToString();
                    //string GROUPID_ADD0 = new string('0', 5-GROUPID.Length);
                    //// Debug.WriteLine($"Add 0 : {GROUPID_ADD0}");
                    //Debug.WriteLine($"GROUP Send:{GROUPID_ADD0 + GROUPID}");
                    //byte[] bytes_group = Encoding.ASCII.GetBytes(GROUPID_ADD0 + GROUPID);
                    //byte[] group_send = { 0x02, 0x67, 0x42, 0x47, bytes_group[0], bytes_group[1],
                    //                    bytes_group[2],bytes_group[3],bytes_group[4], 0x31 ,0x30 ,0x03 };
                    //mainForm.Service.SocketSendData(group_send);
                    if (mainForm.Readio_Mode == "NXDN")
                    {
                        string GROUPID = Properties.Settings.Default.GROUPID.ToString();
                        string GROUPID_ADD0 = new string('0', 5 - GROUPID.Length);
                        // Debug.WriteLine($"Add 0 : {GROUPID_ADD0}");
                        Debug.WriteLine($"GROUP Send:{GROUPID_ADD0 + GROUPID}");
                        byte[] bytes_group = Encoding.ASCII.GetBytes(GROUPID_ADD0 + GROUPID);
                        byte[] group_send = { 0x02, 0x67, 0x42, 0x47, bytes_group[0], bytes_group[1],
                                        bytes_group[2],bytes_group[3],bytes_group[4], 0x31 ,0x30 ,0x03 };
                        mainForm.Service.SocketSendData(group_send);
                    }
                    else 
                    {
                        string GROUPID = Properties.Settings.Default.GROUPID.ToString();
                        string GROUPID_ADD0 = new string('0', 8 - GROUPID.Length);
                        // Debug.WriteLine($"Add 0 : {GROUPID_ADD0}");
                        Debug.WriteLine($"GROUP Send:{GROUPID_ADD0 + GROUPID}");
                        byte[] bytes_group = Encoding.ASCII.GetBytes(GROUPID_ADD0 + GROUPID);
                        byte[] group_send = { 0x02, 0x78, 0x42, 0x47,bytes_group[0], bytes_group[1],
                                        bytes_group[2],bytes_group[3],bytes_group[4],bytes_group[5],
                                        bytes_group[6],bytes_group[7], 0x31 ,0x30 ,0x03 };
                        mainForm.Service.SocketSendData(group_send);
                        Debug.WriteLine($"{Encoding.ASCII.GetString(group_send)}");
                    }
                }
                //Byte[] byteDateLine = { 0x02, 0x41, 0x30, 0x31, 0x03 };
                //mainForm.Service.SocketSendData(byteDateLine);
                pttCounter++;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        public void PTTUp()
        {
            try
            {
                PTTStyleChange(NameType.EndCall);
                StopRecording();
                //mainForm.Service.SocketSendData(
                //    new RadioData(NameType.EndCall.ToString(), ActionType.set.ToString(), labelSendUID_Display.Text.ToString())
                //);
                Byte[] byteDateLine = { 0x02, 0x43, 0x30, 0x32, 0x03 };
                mainForm.Service.SocketSendData(byteDateLine);
                pttCounter = 0;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        public void PTTButton_KeyDown(object sender, KeyEventArgs e)
        {
            if (pttCounter == 0 && e.KeyCode == Keys.Space)
            {
                PTTDown();
            }
        }

        private void PTTButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (pttCounter == 0)
            {
                PTTDown();
            }
        }

        public void PTTButton_KeyUp(object sender, KeyEventArgs e)
        {
            if (pttCounter > 0)
            {
                PTTUp();
            }
        }

        private void PTTButton_MouseUp(object sender, MouseEventArgs e)
        {
            if (pttCounter > 0)
            {
                PTTUp();
            }
        }

        private void channelUp_Click(object sender, EventArgs e)
        {
            try
            {
                int g = Properties.Settings.Default.UID;
                g++;
                Properties.Settings.Default.UID = g;
                Properties.Settings.Default.profile_select = profiles.Find(obj => obj.UID == g).Id;
                Properties.Settings.Default.Save();

                InitData();
                mainForm.Service.SocketSendData(
                   new RadioData(NameType.GROUP.ToString(), ActionType.set.ToString(), "G" + Properties.Settings.Default.GROUPID)
                );
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        private void channelDown_Click(object sender, EventArgs e)
        {
            try
            {
                int g = Properties.Settings.Default.UID;
                if (g > 1)
                {
                    g--;
                    Properties.Settings.Default.UID = g;
                    Properties.Settings.Default.profile_select = profiles.Find(obj => obj.UID == g).Id;
                    Properties.Settings.Default.Save();

                    InitData();
                    mainForm.Service.SocketSendData(
                         new RadioData(NameType.GROUP.ToString(), ActionType.set.ToString(), "G" + Properties.Settings.Default.GROUPID)
                    );
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        private void Speaker_Click(object sender, EventArgs e)
        {
            this.KeyDown += new KeyEventHandler(this.PTTButton_KeyDown);
            this.KeyUp += new KeyEventHandler(this.PTTButton_KeyUp);
        }

        private void PTTButton_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void PTTUserControl_Click(object sender, EventArgs e)
        {
            this.KeyDown += new KeyEventHandler(this.PTTButton_KeyDown);
            this.KeyUp += new KeyEventHandler(this.PTTButton_KeyUp);
        }

        private void EmergencyButton_Click(object sender, EventArgs e)
        {
            // mainForm.Service.SocketSendData(new RadioData(NameType.EMERGENCY.ToString(), ActionType.set.ToString(), labelUUID.Text.ToString()));

        }
    }

    public class MuteEventArgs : EventArgs
    {
        public MuteEventArgs(bool muted)
        {
            Muted = muted;
        }

        public bool Muted { get; private set; }
    }

    public class VolumeEventArgs : EventArgs
    {
        public VolumeEventArgs(float volume)
        {
            Volume = volume;
        }

        public float Volume { get; private set; }
    }
}
