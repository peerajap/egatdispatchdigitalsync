﻿using DigitalSync.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DigitalSync
{
    public partial class ButtonFuncUserControl : UserControl
    {
        public String FunctionName;

        public Profile profile;
        public ProfileGroup profileGroup;
        PTTUserControl pttUserControl;

        public String ButtonMode = "UID";

        public Label G { get { return this.labelG; } set { this.labelG = value; } }
        public Label FUNCTION { get { return this.labelFunctionNumber; } set { this.labelFunctionNumber = value; } }
        public Label NAME { get { return this.labelName; } set { this.labelName = value; } }

        public delegate void OnSelectChange();
        public event OnSelectChange selectListening;

        public ButtonFuncUserControl(PTTUserControl pttUserControl, Profile profile)
        {
            this.pttUserControl = pttUserControl;
            this.profile = profile;
            this.ButtonMode = "UID";
            InitializeComponent();
            InitTextDisplay();

        }

        public ButtonFuncUserControl(PTTUserControl pttUserControl, ProfileGroup profileGroup)
        {
            this.pttUserControl = pttUserControl;
            this.profileGroup = profileGroup;
            this.ButtonMode = "GROUP";
            InitializeComponent();
            InitTextDisplay();

        }

        public void InitTextDisplay()
        {
            try
            {
                if (profile != null) 
                {
                    SetSelect(Properties.Settings.Default.profile_select == profile.Id);
                }
                
            }
            catch 
            {
                SetSelect(Properties.Settings.Default.profile_select == profileGroup.Id);
            }
            this.labelName.Parent.Padding = new Padding((labelName.Parent.Width - labelName.Width) / 2, 0, 0, 0);
        }

        private void edit_Click(object sender, EventArgs e)
        {
            DialogResult result = ProfileUpdate.MSGShow(this);
            try
            {
                if (result == DialogResult.OK)
                {
                    if (ButtonMode == "UID")
                    {
                        G.Text = "G" + profile.Group;
                        FUNCTION.Text = "UID" + (profile.UID).ToString();
                        NAME.Text = profile.Name;
                        InitTextDisplay();
                    }
                    else 
                    {
                        G.Text = "G" + profileGroup.Group;
                        // FUNCTION.Text = "UID" + (profileGroup.UID).ToString();
                        NAME.Text = profileGroup.Name;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        private void ButtonFuncUserControl_Resize(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
        }

        private void SetSelect(bool isSelect)
        {
            int gDisplay = Properties.Settings.Default.GROUPID;
            this.Body.BackgroundImage = (isSelect && gDisplay == profile.Group) ? Properties.Resources.border_function_blue
                : Properties.Resources.border_function_gray;
            this.FUNCTION.ForeColor = (isSelect && gDisplay == profile.Group) ? Color.White : Color.FromArgb(80, 80, 80);
            this.G.ForeColor = (isSelect && gDisplay == profile.Group) ? Color.White : Color.FromArgb(80, 80, 80);
            this.NAME.ForeColor = (isSelect && gDisplay == profile.Group) ? Color.White : Color.FromArgb(80, 80, 80);
        }

        private void button_Click(object sender, EventArgs e)
        {
            if (profile != null)
            {
                Properties.Settings.Default.profile_select = profile.Id;
                Properties.Settings.Default.UID = profile.UID;
                Debug.WriteLine($"button_Click ID:{profile.Id}");
            }
            else 
            {
                Properties.Settings.Default.profile_select = profileGroup.Id;
                Properties.Settings.Default.UID = profileGroup.Id;
            }
            
            
            Properties.Settings.Default.Save();
            selectListening();
            
        }
    }
}
