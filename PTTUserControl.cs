﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RadioNetworkDesktop
{
    public partial class PTTUserControl : UserControl
    {
        private int func_record = 1;
        private List<RecordUserControl> recordPanels = new List<RecordUserControl>();

        public PTTUserControl()
        {
            InitializeComponent();
            InitTextDisplay();
            InitFuncRecord();
        }

        private void InitFuncRecord()
        {
            for (int ri = 0; ri < 10; ri++)
            {
                List<ButtonFuncUserControl> buttons = new List<ButtonFuncUserControl>();
                for (int bi = 0; bi < 3; bi++)
                {
                    ButtonFuncUserControl buttonFuncUser = new ButtonFuncUserControl();
                    buttonFuncUser.CH.Text = "CH" + ((Int32)(bi + 1) + (ri * 3)).ToString();
                    buttonFuncUser.FUNCTION.Text = "F" + ((Int32)(bi + 1) + (ri * 3)).ToString();
                    buttonFuncUser.NAME.Text = "NAME";
                    buttonFuncUser.InitTextDisplay();
                    buttons.Add(buttonFuncUser);
                }

                recordPanels.Add(new RecordUserControl(buttons));
            }
        }

        private void InitTextDisplay()
        {
            this.labelUID_Display.Padding = new Padding((labelUID_Display.Parent.Width - labelUID_Display.Width) / 2, 0, 0, 0);
            this.labelName_Display.Padding = new Padding((labelName_Display.Parent.Width - labelName_Display.Width) / 2, 0, 0, 0);
            this.labelTime_Display.Padding = new Padding((labelTime_Display.Parent.Width - labelTime_Display.Width) / 2, 0, 0, 0);
            this.labelPTTStatus_Display.Padding = new Padding((labelPTTStatus_Display.Parent.Width - labelPTTStatus_Display.Width) / 2, 0, 0, 0);
        }

        private void ChangeRecordFunction()
        {
            this.panelFunction.Controls.Clear();
            for (int ri = func_record -1 ; ri >= 0 ; ri--)
            {
                this.panelFunction.Invoke(new MethodInvoker(delegate { this.panelFunction.Controls.Add(recordPanels[ri]); }));
            }
        }

        private void PTTUserControl_SizeChanged(object sender, EventArgs e)
        {
            if (recordPanels.Count() > 0)
            {
                int h_Func = this.panelFunction.Height;
                int h_Record = this.recordPanels[0].Height;

                int record = h_Func / h_Record;
                if (record != func_record)
                {
                    func_record = record;

                    // new object here
                    ChangeRecordFunction();
                    Console.WriteLine("PTT Size change record : " + func_record.ToString());
                }
            }
        }

        private void PTTButton_MouseDown(object sender, MouseEventArgs e)
        {

        }

        private void PTTButton_MouseUp(object sender, MouseEventArgs e)
        {

        }
    }
}
