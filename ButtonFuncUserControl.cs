﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RadioNetworkDesktop
{
    public partial class ButtonFuncUserControl : UserControl
    {
        public String FunctionName;

        public Label CH { get { return this.labelCH; } set { this.labelCH = value; } }
        public Label FUNCTION { get { return this.labelFunctionNumber; } set { this.labelFunctionNumber = value; } }
        public Label NAME { get { return this.labelName; } set { this.labelName = value; } }

        public ButtonFuncUserControl()
        {
            InitializeComponent();
            InitTextDisplay();
        }

        public void InitTextDisplay()
        {
            this.labelName.Parent.Padding = new Padding((labelName.Parent.Width - labelName.Width) / 2, 0, 0, 0);

        }

        private void edit_Click(object sender, EventArgs e)
        {

        }

        private void ButtonFuncUserControl_Resize(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
        }

    }
}
