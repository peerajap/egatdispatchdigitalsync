﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RadioNetworkDesktop
{
    public partial class RecordUserControl : UserControl
    {
        public List<ButtonFuncUserControl> buttonFuncUserControls;

        public RecordUserControl(List<ButtonFuncUserControl> buttonFuncUserControls)
        {
            InitializeComponent();

            this.buttonFuncUserControls = buttonFuncUserControls;
            this.panel0.Controls.Add(buttonFuncUserControls[0]);
            this.panel1.Controls.Add(buttonFuncUserControls[1]);
            this.panel2.Controls.Add(buttonFuncUserControls[2]);

        }

        private void RecordUserControl_Resize(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Top;
        }
    }
}
