﻿using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.WindowsForms;
using RadioNetworkDesktop.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RadioNetworkDesktop
{
    public partial class MapUserControl : UserControl
    {
        public delegate void LocationChange(PointLatLng pointLatLng);

        public MapUserControl()
        {
            InitializeComponent();
            InitializeMapControl();
        }

        private void InitializeMapControl()
        {
            if (!GMapControl.IsDesignerHosted)
            {
                // set cache mode only if no internet avaible
                if (!Stuff.PingNetwork("pingtest.com"))
                {
                    map.Manager.Mode = AccessMode.CacheOnly;
                    //MessageBox.Show("No internet connection available, going to CacheOnly mode.", "GMap.NET - Demo.WindowsForms", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    map.Manager.Mode = AccessMode.ServerAndCache;
                }

                // config map         
                map.MapProvider = GMapProviders.GoogleMap;
                map.MinZoom = 12;
                map.MaxZoom = 16;
                map.Zoom = 15;
                map.Bearing = 0;

                map.Position = new PointLatLng(13.781242044308085, 100.57502300340474);
                map.DragButton = MouseButtons.Left;
            }
        }

        private void map_Resize(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
        }
    }
}
